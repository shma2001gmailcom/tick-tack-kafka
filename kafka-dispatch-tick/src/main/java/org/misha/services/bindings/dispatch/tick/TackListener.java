package org.misha.services.bindings.dispatch.tick;

import lombok.extern.slf4j.Slf4j;
import org.misha.services.bindings.dispatch.AbstractListener;
import org.misha.services.bindings.dispatch.Sender;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TackListener extends AbstractListener {

    TackListener(Sender tickSender) {
        //addObserver(tickSender);
    }
}
