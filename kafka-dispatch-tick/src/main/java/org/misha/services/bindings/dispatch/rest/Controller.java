package org.misha.services.bindings.dispatch.rest;

import lombok.extern.slf4j.Slf4j;
import org.misha.services.bindings.dispatch.Message;
import org.misha.services.bindings.dispatch.tick.TickSender;
import org.misha.services.bindings.test.server.TackService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.stream.IntStream;

@Slf4j
@RestController
public class Controller {
    private final TackService tackService;
    private final TickSender tickSender;

    public Controller(TickSender tickSender, TackService tackService) {
        this.tickSender = tickSender;
        this.tackService = tackService;
    }

    @GetMapping("/start")
    public void start() throws InterruptedException, IllegalAccessException, IOException, InvocationTargetException {
        IntStream.range(0, 1).forEach(i -> {
            try {
                log.debug("Result={}", tackService.returnSomething(i, "s" + i));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        });
        Message<String> m = new Message<>();
        m.setCorrelationId(UUID.randomUUID().toString());
        m.setMessageType("tick");
        m.setPayload("{\"serviceName\": \"org.misha.services.bindings.dispatch.test.Config.AnotherService\"," +
                "\"annotations\": null," +
                "\"returnType\": \"org.misha.services.bindings.test.server.SomeBean\"," +
                "\"methodName\": \"getSomeBean\"," +
                "\"args\": [{\"type\":\"java.lang.Integer\",\"value\":\"0\"},{\"type\":\"java.lang.String\",\"value\":\"s0\"}]," +
                "\"modifiers\": \"public\"}");
        tickSender.send(m);
    }
}
