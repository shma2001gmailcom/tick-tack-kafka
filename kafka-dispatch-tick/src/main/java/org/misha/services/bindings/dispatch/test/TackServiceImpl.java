package org.misha.services.bindings.dispatch.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.misha.services.bindings.dispatch.Message;
import org.misha.services.bindings.dispatch.tick.TackListener;
import org.misha.services.bindings.dispatch.tick.TickSender;
import org.misha.services.bindings.kafka.RemoteMethodInfo;
import org.misha.services.bindings.kafka.TypedValueInfo;
import org.misha.services.bindings.test.server.SomeBean;
import org.misha.services.bindings.test.server.TackService;
import org.misha.services.bindings.test.server.impl.SomeBeanImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.concurrent.CompletableFuture.supplyAsync;

@Slf4j
@Component
public class TackServiceImpl implements TackService {
    @Autowired
    TickSender tickSender;
    @Autowired
    TackListener tackListener;
    @Autowired
    ObjectMapper mapper;

    @Override
    public SomeBean returnSomething(final Integer i,
                                    final String s
    ) throws IOException, InterruptedException, ExecutionException, InvocationTargetException,
            IllegalAccessException, TimeoutException {
        RemoteMethodInfo remoteMethodInfo
                = RemoteMethodInfo.builder()
                                  .serviceName("org.misha.services.bindings.dispatch.test.Config.TackService")
                                  .methodName("returnSomething")
                                  .args(Lists.newArrayList(new TypedValueInfo("java.lang.Integer", "" + i),
                                                           new TypedValueInfo("java.lang.String", s))).build();
        String payload = mapper.writeValueAsString(remoteMethodInfo);
        final Message<String> request = new Message<>();
        request.setMessageType("request for SomeBean");
        request.setSender(getClass().getSimpleName());
        request.setPayload(payload);
        tickSender.send(request);
        Message<?> response = tickSender.messageReceived(request);
        while (response == null) {
            synchronized (this) {
                try {
                    wait(10);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(e);
                }
            }
        }
        notifyAll();
        String responseJson = (String) response.getPayload();
        log.debug("\n\n\n\nResponse received: {}", responseJson);
        final SomeBeanImpl someBean = mapper.readValue(responseJson, SomeBeanImpl.class);
        log.debug("SomeBean: {}: ", someBean);
        return null;
    }
}
