package org.misha.services.bindings.dispatch.test;

import org.misha.services.bindings.test.server.TackService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    TackService tackService() {
        return new TackServiceImpl();
    }
}
