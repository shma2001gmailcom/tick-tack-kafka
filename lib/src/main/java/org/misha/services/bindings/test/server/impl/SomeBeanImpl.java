package org.misha.services.bindings.test.server.impl;

import org.misha.services.bindings.test.server.SomeBean;
import org.springframework.stereotype.Component;

public class SomeBeanImpl implements SomeBean {
    private final Integer i;
    private final String s;

    public SomeBeanImpl(int i, String s) {
        this.i = i;
        this.s = s;
    }

    @Override
    public Integer getI() {
        return i;
    }

    @Override
    public String getS() {
        return s;
    }

    @Override
    public String toString() {
        return "SomeBeanImpl{" +
                "i=" + i +
                ", s='" + s + '\'' +
                '}';
    }
}
