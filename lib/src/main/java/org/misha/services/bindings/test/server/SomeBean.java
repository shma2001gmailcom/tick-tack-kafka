package org.misha.services.bindings.test.server;

public interface SomeBean {
    Integer getI();

    String getS();
}
