package org.misha.services.bindings.kafka;

import org.misha.services.bindings.core.ServiceBinder;
import org.misha.services.bindings.exceptions.NotImplementedException;

import javax.validation.constraints.NotNull;

public class KafkaServiceBinder implements ServiceBinder {

    @Override
    public <T> @NotNull T bind(@NotNull Class<T> service, @NotNull T serviceImpl) {
        throw new NotImplementedException();
    }

    @Override
    public <T> @NotNull T bindClient(@NotNull Class<? extends T> service) {
        throw new NotImplementedException();
    }
}