#!/usr/bin/env bash

kafka='/home/mshevelin/kafka'

pwd=`pwd`

bash ${kafka}/bin/zookeeper-server-start.sh ${kafka}/config/zookeeper.properties

cd ${pwd}
