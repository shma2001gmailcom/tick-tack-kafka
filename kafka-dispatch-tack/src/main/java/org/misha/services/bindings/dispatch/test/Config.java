package org.misha.services.bindings.dispatch.test;

import org.misha.services.bindings.test.server.SomeBean;
import org.misha.services.bindings.test.server.impl.SomeBeanImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.util.function.BiFunction;
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Configuration
public class Config {
    @Bean
    public org.misha.services.bindings.test.server.TackService tackService() {
        return new TackService(anotherService());
    }

    @Bean
    public AnotherService anotherService() {
        return new AnotherService();
    }

    @Component
    public static class TackService implements org.misha.services.bindings.test.server.TackService {
        private final AnotherService anotherService;

        public TackService(final AnotherService anotherService) {
            this.anotherService = anotherService;
        }

        /**
         * Used by a caller on another JVM as a return value
         *
         * @param i sample int parameter
         * @param s sample String parameter
         * @return SomeBean containing the parameters
         */
        @SuppressWarnings("unused")
        public SomeBean returnSomething(Integer i, String s) {
            return anotherService.getSomeBean(i, s);
        }
    }

    public static class AnotherService {

        public SomeBean getSomeBean(int i, String s) {
            return new SomeBeanImpl(i, s);
        }
    }

}
