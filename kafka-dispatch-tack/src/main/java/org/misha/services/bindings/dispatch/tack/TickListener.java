package org.misha.services.bindings.dispatch.tack;

import org.misha.services.bindings.dispatch.AbstractListener;
import org.misha.services.bindings.dispatch.Sender;
import org.springframework.stereotype.Component;

@Component
class TickListener extends AbstractListener {

    public TickListener(Sender tackSender) {addObserver(tackSender);}
}
