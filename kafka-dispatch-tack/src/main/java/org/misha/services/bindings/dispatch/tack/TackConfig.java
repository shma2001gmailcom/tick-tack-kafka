package org.misha.services.bindings.dispatch.tack;

import org.misha.services.bindings.dispatch.Listener;
import org.misha.services.bindings.dispatch.Sender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TackConfig {

    @Bean
    Sender sender() {
        return new TackSender();
    }

    @Bean
    Listener listener() {
        return new TickListener(sender());
    }
}
